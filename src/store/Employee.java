/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;


/**
 *
 * @author USUARIO
 */
public class Employee extends Person {
    private String username;
    private String password;
    
    public Employee() {
    }

    public Employee(
        long id, 
        String firstName, 
        String lastName, 
        Gender gender, 
        String username, 
        String password
    ) throws Exception {
        super(id, firstName, lastName, gender);
        
        if (username == null || username.trim().equals("")) {
            throw new IllegalArgumentException("El Usuario no debe ser vacio");
        }
        
        if (password == null || password.trim().equals("")) {
            throw new IllegalArgumentException("La contraseña no debe ser vacia");
        }
        
        this.username = username;
        this.password = password;
    }
    
    @Override
    public long getId() {
        return super.getId();
    }

    @Override
    public String getFirstName() {
        return super.getFirstName();
    }

    @Override
    public String getLastName() {
        return super.getLastName();
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) throws Exception {
        if (username == null || username.trim().equals("")) {
            throw new IllegalArgumentException("El Usuario no debe ser vacio");
        }
        
        this.username = username;
    }

    public void setPassword(String password) throws Exception {
        if (password == null || password.trim().equals("")) {
            throw new IllegalArgumentException("La contraseña no debe ser vacia");
        }
        
        this.password = password;
    }

}