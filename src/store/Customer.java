/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;


/**
 *
 * @author USUARIO
 */
public class Customer extends Person{
    private long phoneNumber;
    private String mail;
    private long points;
    
    public Customer() {
    }
    
    public Customer(
        long id, 
        String firstName, 
        String lastName, 
        Gender gender, 
        long phoneNumber, 
        String mail) throws Exception {
        
        super(id, firstName, lastName, gender);
        
        String tel = Long.toString(phoneNumber); 
        if (!(tel.length() == 7 || tel.length() == 10)) {
            throw new IllegalArgumentException("El número de telefono es invalido.");
        }
        
        if (mail == null || mail.trim().equals("")) {
            throw new IllegalArgumentException("El correo no puede ser nulo ni vacio.");
        }
        
        if (points < 0) {
            throw new IllegalArgumentException("Los puntos no pueden ser menr a 0.");
        }
        
        this.phoneNumber = phoneNumber;
        this.mail = mail;
        this.points = 0;
    }

    public Customer(
        long id, 
        String firstName, 
        String lastName, 
        Gender gender, 
        long phoneNumber, 
        String mail, 
        long points) throws Exception {
        
        super(id, firstName, lastName, gender);
        
        String tel = Long.toString(phoneNumber); 
        if (!(tel.length() == 7 || tel.length() == 10)) {
            throw new IllegalArgumentException("El número de telefono es invalido.");
        }
        
        if (mail == null || mail.trim().equals("")) {
            throw new IllegalArgumentException("El correo no puede ser nulo ni vacio.");
        }
        
        if (points < 0) {
            throw new IllegalArgumentException("Los puntos no pueden ser menr a 0.");
        }
        
        this.phoneNumber = phoneNumber;
        this.mail = mail;
        this.points = points;
    }

    @Override
    public long getId() {
        return super.getId();
    }

    @Override
    public String getFirstName() {
        return super.getFirstName();
    }

    @Override
    public String getLastName() {
        return super.getLastName();
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public String getMail() {
        return mail;
    }

    public long getPoints() {
        return points;
    }

    public void setPhoneNumber(long phoneNumber) throws Exception {
        String tel = Long.toString(phoneNumber); 
        if (!(tel.length() == 7 || tel.length() == 10)) {
            throw new IllegalArgumentException("El número de telefono es invalido.");
        }
        
        this.phoneNumber = phoneNumber;
    }

    public void setMail(String mail) throws Exception {
        if (mail == null || mail.trim().equals("")) {
            throw new IllegalArgumentException("El correo no puede ser nulo ni vacio.");
        }
        
        this.mail = mail;
    }

    public void setPoints(long points) throws Exception {
        if (points < 0) {
            throw new IllegalArgumentException("Los puntos no pueden ser menr a 0.");
        }
        
        this.points = points;
    }
    
}
