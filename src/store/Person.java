/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 *
 * @author USUARIO
 */
public class Person {
    private long id;
    private String firstName;
    private String lastName;
    private Gender gender;
    
    public Person() {
    }
    
    public Person(long id, String firstName, String lastName, Gender gender) throws Exception {
        if (id <= 0) {
            throw new IllegalArgumentException("El número de identificación de la persona no puede ser nulo.");
        }
        
        if (validateFirstNameOrLastName(firstName) == false) {
            throw new IllegalArgumentException("El nombre introducido es invalido. Se introdujo: "+firstName);
        }
        
        if (validateFirstNameOrLastName(lastName) == false) {
            throw new IllegalArgumentException("El apellido introducido es invalido. Se introdujo: "+lastName);
        }
        
        if (gender == null) {
            throw new IllegalArgumentException("El Genero no puede ser nulo.");
        }
        
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
    }
    
    private boolean validateFirstNameOrLastName(String firstNameOrLastName) {
        Pattern patternValidator = Pattern.compile(
                "[a-zA-Z_0-9]*");
        Matcher comparator = patternValidator.matcher(firstNameOrLastName);
        return comparator.find();
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setId(long id) throws Exception {
        if (id <= 0) {
            throw new IllegalArgumentException("El número de identificación de la persona no puede ser nulo.");
        }
        
        this.id = id;
    }

    public void setFirstName(String firstName) throws Exception {
        if (validateFirstNameOrLastName(firstName) == false) {
            throw new IllegalArgumentException("El nombre introducido es invalido. Se introdujo: "+firstName);
        }
        
        this.firstName = firstName;
    }

    public void setLastName(String lastName) throws Exception {
        if (validateFirstNameOrLastName(lastName) == false) {
            throw new IllegalArgumentException("El nombre introducido es invalido. Se introdujo: "+lastName);
        }
        
        this.lastName = lastName;
    }

    public void setGender(Gender gender) throws Exception {
        if (gender == null) {
            throw new IllegalArgumentException("El Genero no puede ser nulo.");
        }
        
        this.gender = gender;
    }
    
}

