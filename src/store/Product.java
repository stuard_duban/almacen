/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;


/**
 *
 * @author USUARIO
 */
public class Product {
    private long code;
    private String name;
    private int cost;
    private ProductType type;

    /**
     *
     * @param code
     * @param name
     * @param cost
     * @param type
     * @throws Exception
     */
    public Product(long code, String name, int cost, ProductType type) throws Exception {
        if (code < 0) {
            throw new IllegalArgumentException("El código del producto no puede ser negativo.");
        }
        
        if (name == null || name.trim().equals("")) {
            throw new IllegalArgumentException("El campo para nombre debe tener información [Recuerde incluir una dirección de nombre valida @]");
        }
        
        if (cost < 0) {
            throw new IllegalArgumentException("El costo del producto no puede ser negativo.");
        }
        
        if (type == null) {
            throw new IllegalArgumentException("El tipo del producto no puede ser nulo.");
        }
        
        this.code = code;
        this.name = name;
        this.cost = cost;
        this.type = type;
    }

    public long getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public int getCost() {
        return cost;
    }

    public ProductType getType() {
        return type;
    }

    public void setCode(long code) throws Exception {
        if (code < 0) {
            throw new IllegalArgumentException("El código del producto no puede ser negativo.");
        }
        
        this.code = code;
    }

    public void setName(String name) throws Exception {
        if (name == null || name.trim().equals("")) {
            throw new IllegalArgumentException("El campo para nombre debe tener información");
        }
        
        this.name = name;
    }

    public void setCost(int cost) throws Exception {
        if (cost < 0) {
            throw new IllegalArgumentException("El costo del producto no puede ser negativo.");
        }
        
        this.cost = cost;
    }

    public void setType(ProductType type) throws Exception {
        if (type == null) {
            throw new IllegalArgumentException("El tipo del producto no puede ser nulo.");
        }
        
        this.type = type;
    }
    
    public String toString() {
        return this.name;
    }

}
