/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistence.ProductTypeJpaController;


/**
 *
 * @author Stuard Romero
 */
public class Store {
    private String nit;
    private String name;
    private String address;
    
//    private LinkedList<ProductType> productTypes;
    // Definir como atributos objetos de tipo EntityManagerFactory y PersonaJpaController.
    private EntityManagerFactory factory = Persistence.createEntityManagerFactory("AlmacenPU"); // Crear solo un objeto de este tipo
    private ProductTypeJpaController productTypeJpaController = new ProductTypeJpaController(factory);

    private LinkedList<Buying> buyings;
    private LinkedList<Product> products;
    private LinkedList<Customer> customers;
    private LinkedList<Employee> employees;
    
    public Store() {
        this.products = new LinkedList<Product>();
        this.employees = new LinkedList<Employee>();
        this.customers = new LinkedList<Customer>();
        this.buyings = new LinkedList<Buying>();
    }
    
    public Store(String nit, String name, String address)
    {
        if (nit == null || nit.trim().equals("")) {
            throw new IllegalArgumentException("El nit no puede ser nulo ni vacio.");
        }
        
        if (name == null || name.trim().equals("")) {
            throw new IllegalArgumentException("El nombre no puede ser nulo ni vacio.");
        }
        
        if (address == null || address.trim().equals("")) {
            throw new IllegalArgumentException("La dirección no puede ser nula ni vacia.");
        }
        
        this.nit = nit;
        this.name = name;
        this.address = address;
        this.products = new LinkedList<>();
        this.employees = new LinkedList<>();
        this.customers = new LinkedList<>();
        this.buyings = new LinkedList<>();
    }

    public Store(
        String nit, 
        String name, 
        String address, 
        LinkedList<Buying> buyings, 
        LinkedList<ProductType> productsTypes, 
        LinkedList<Product> products, 
        LinkedList<Customer> customers, 
        LinkedList<Employee> employees
    ) throws Exception {
        
        if (nit == null || nit.trim().equals("")) {
            throw new IllegalArgumentException("El nit no puede ser nulo ni vacio.");
        }
        
        if (name == null || name.trim().equals("")) {
            throw new IllegalArgumentException("El nombre no puede ser nulo ni vacio.");
        }
        
        if (address == null || address.trim().equals("")) {
            throw new IllegalArgumentException("La dirección no puede ser nula ni vacia.");
        }
        
        if (buyings == null) {
            throw new IllegalArgumentException("La compra no puede ser nula ni vacia.");
        }
        
//        if (productTypes == null) {
//            throw new IllegalArgumentException("El tipo de producto no puede ser nulo ni vacio.");
//        }
        
        if (products == null) {
            throw new IllegalArgumentException("El producto no puede ser nulo ni vacio.");
        }
        
        if (customers == null) {
            throw new IllegalArgumentException("El cliente no puede ser nulo ni vacio.");
        }
        
        if (employees == null) {
            throw new IllegalArgumentException("El empleado no puede ser nulo ni vacio.");
        }
        
        this.nit = nit;
        this.name = name;
        this.address = address;
        this.buyings = buyings;
//        this.productTypes = productTypes;
        this.products = products;
        this.customers = customers;
        this.employees = employees;
    }

    public String getNit() {
        return nit;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public LinkedList<Buying> getBuying() {
        return buyings;
    }

    public List<ProductType> getProductTypes() {
        return productTypeJpaController.findProductTypeEntities();
    }

    public LinkedList<Product> getProducts() {
        return products;
    }

    public LinkedList<Customer> getCustomer() {
        return customers;
    }

    public LinkedList<Employee> getEmployees() {
        return employees;
    }

    public void setNit(String nit) {
        if (nit == null || nit.trim().equals("")) {
            throw new IllegalArgumentException("El nit no puede ser nulo ni vacio.");
        }
        
        this.nit = nit;
    }

    public void setName(String name) {
        if (name == null || name.trim().equals("")) {
            throw new IllegalArgumentException("El nombre no puede ser nulo ni vacio.");
        }
        
        this.name = name;
    }

    public void setAddress(String address) {
        if (address == null || address.trim().equals("")) {
            throw new IllegalArgumentException("La dirección no puede ser nula ni vacia.");
        }
        
        this.address = address;
    }

    public void setBuyings(LinkedList<Buying> buyings) {
        if (buyings == null) {
            throw new IllegalArgumentException("La compra no puede ser nula ni vacia.");
        }
        
        this.buyings = buyings;
    }

//    public void setProductType(List<ProductType> productType) {
//        if (productType == null) {
//            throw new IllegalArgumentException("El tipo de producto no puede ser nulo ni vacio.");
//        }
//        
//        this.productTypeJpaController = productType;
//    }

    public void setProducts(LinkedList<Product> products) {
        if (products == null) {
            throw new IllegalArgumentException("El producto no puede ser nulo ni vacio.");
        }
        
        this.products = products;
    }

    public void setCustomers(LinkedList<Customer> customers) {
        if (customers == null) {
            throw new IllegalArgumentException("El cliente no puede ser nulo ni vacio.");
        }
        
        this.customers = customers;
    }

    public void setEmployees(LinkedList<Employee> employees) {
        if (employees == null) {
            throw new IllegalArgumentException("El empleado no puede ser nulo ni vacio.");
        }
        
        this.employees = employees;
    }
    
    public void addProductType(ProductType productType) throws Exception {
        if (productType == null) {
            throw new IllegalArgumentException("El tipo de producto no puede ser nulo.");
        }
//        if (this.productTypes.contains(productType)) {
//            throw new Exception("Este Tipo de Producto ya existe");
//        }
        this.productTypeJpaController.create(productType);
    }
    

    public void addProduct(Product product) throws Exception {
        if (product == null) {
            throw new IllegalArgumentException("El producto no puede ser nulo.");
        }
        if (this.products.contains(product)) {
            throw new Exception("Este producto ya se encuentra registrado.");
        }
        this.products.add(product);
    }

    public void addEmployee(Employee employee) throws Exception {
        if (employee == null) {
            throw new IllegalArgumentException("El empleado no puede ser nulo");
        }
        
        if (this.employees.contains(employee)) {
            throw new Exception("El empleado ya se encuentra registrado");
        }
        this.employees.add(employee);
    }

    public void addCustomer(Customer customer) throws Exception {
        if (customer == null) {
            throw new IllegalArgumentException("El cliente no puede ser null");
        }
        //Con el metodo contains se hace uso del Equals.
        if (this.customers.contains(customer)) {
            throw new Exception("Este cliente ya está registrado");
        }
        this.customers.add(customer);
    }

    public void addBuying(Buying buying) throws Exception {
        if (buying == null) {
            throw new IllegalArgumentException("La compra no puede ser nula.");
        }
        this.buyings.add(buying);
    }
    
    public Customer searchForCustomer(long id) throws Exception {
        for (Customer customer : customers) {
            if (customer.getId() == id) {
                return customer;
            }
        }
        throw new Exception("No se ha encontrado ningun cliente con la Identificación = " + id + ".");
    }

    public Product searchForProduct(long code) throws Exception {
        for (Product product : products) {
            if (product.getCode() == code) {
                return product;
            }
        }
        throw new Exception("No se ha encontrado ningun producto con el codigo: " + code + ".");
    }

    public Employee searchForEmployee(long id) throws Exception {
        for (Employee employee : employees) {
            if (employee.getId() == id) {
                return employee;
            }
        }
        throw new Exception("No se ha encontrado ningun empleado con la id : " + id);
    }
    
    public Employee searchForEmployeeByUsername(String username) throws Exception {
        for (Employee employee : employees) {
            if (employee.getUsername().equals(username)) {
                return employee;
            }
        }
        throw new Exception("No se ha encontrado ningun empleado con el usuario: " + username);
    }

    public ProductType searchForProductType(String name) throws Exception {
        List<ProductType> productTypes = productTypeJpaController.findProductTypeEntities();
        for (ProductType productType : productTypes) {
            if (productType.getName().equals(name)) {
                return productType;
            }
        }
        throw new Exception("No se ha encontrado ningún Tipo de Producto con el nombre de: " + name + ".");
    }
    
}
