/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

/**
 *
 * @author USUARIO
 */
public class BuyingDetail {
    private int quantity;
    private double buyingCost;
    private double tax;
    private Product product;

    /**
     *
     * @param quantity: cantidad de la compra
     * @param buyingCost: Costo de la compra
     * @param tax: Iva de la compra
     * @param product: Producto del la compra.
     * @throws Exception
     */
    public BuyingDetail(int quantity, double buyingCost, double tax, Product product) throws Exception {
        if (quantity <= 0) {
            throw new IllegalArgumentException("La cantidad no puede ser menor a 0.");
        }
        
        if (buyingCost < 0) {
            throw new IllegalArgumentException("El costo de la compra no puede ser menor a 0.");
        }
        
        if (tax < 0) {
            throw new IllegalArgumentException("El IVA no puede ser menor a 0.");
        }
        
        if (product == null) {
            throw new IllegalArgumentException("El producto no puede ser nulo.");
        }
        this.quantity = quantity;
        this.buyingCost = buyingCost;
        this.tax = tax;
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getBuyingCost() {
        return buyingCost;
    }

    public double getTax() {
        return tax;
    }

    public Product getProduct() {
        return product;
    }

    public void setQuantity(int quantity) throws Exception {
        if (quantity <= 0) {
            throw new IllegalArgumentException("La cantidad no puede ser menor a 0.");
        }
        
        this.quantity = quantity;
    }

    public void setBuyingCost(double buyingCost) throws Exception {
        if (buyingCost < 0) {
            throw new IllegalArgumentException("El costo de la compra no puede ser menor a 0.");
        }
        
        this.buyingCost = buyingCost;
    }

    public void setTax(double tax) throws Exception {
        if (tax < 0) {
            throw new IllegalArgumentException("El IVA no puede ser menor a 0.");
        }
        
        this.tax = tax;
    }

    public void setProduct(Product product) throws Exception {
        if (product == null) {
            throw new IllegalArgumentException("El producto no puede ser nulo.");
        }
        
        this.product = product;
    }
    
}
