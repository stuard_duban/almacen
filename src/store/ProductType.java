/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author USUARIO
 */
@Entity
public class ProductType implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long pk; 

    @Column(nullable = false, length = 100, unique = true)
    private String name;
    
    @Column(nullable = false)
    private double taxPercent;
    
    public ProductType() {
       this.taxPercent = 0;
    }

    public ProductType(String name, double taxPercent) throws Exception {
        if (name == null || name.trim().equals("")) {
            throw new IllegalArgumentException("El campo para nombre debe tener información");
        }
        
        if (taxPercent < 0) {
            throw new IllegalArgumentException("El Iva no puede ser negativo.");
        }
        
        this.name = name;
        this.taxPercent = taxPercent;
    }
    
    public long getPk() {
        return pk;
    }

    public void setPk(long pk) {
        this.pk = pk;
    }

    public String getName() {
        return name;
    }

    public double getTaxPercent() {
        return taxPercent;
    }

    public void setName(String name) throws Exception {
        if (name == null || name.trim().equals(""))  {
            throw new IllegalArgumentException("El campo para nombre debe tener información");
        }
        
        this.name = name;
    }

    public void setTaxPercent(double taxPercent) throws Exception {
        if (taxPercent < 0) {
            throw new IllegalArgumentException("El Iva no puede ser negativo.");
        }
        
        this.taxPercent = taxPercent;
    }
    
    public String toString() {
        return this.name;
    }

}
