/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public class Buying {
    private Employee employee;
    private Customer customer;
    private LinkedList<BuyingDetail> buyingDetail;
    private LocalDate date;
    
    public Buying() {
        this.customer = null;
        this.date = LocalDate.now();
    }

    public Buying(Employee employee, Customer customer, LinkedList<BuyingDetail> buyingDetail) throws Exception {
        if (employee == null) {
            throw new IllegalArgumentException("El Empleado no puede ser nulo.");
        }
        
        if (customer == null) {
            throw new IllegalArgumentException("El Cliente no puede ser nulo.");
        }
        
        if (buyingDetail == null) {
            throw new IllegalArgumentException("Los Detalles de compras no pueden ser nulos.");
        }
        
        this.employee = employee;
        this.customer = customer;
        this.buyingDetail = buyingDetail;
        this.date = LocalDate.now();
    }
    
    public Buying(Employee employee) throws Exception {
        //Cuando ya está implementado arriba en otro constructor:
        this();
        this.employee = employee;
        this.buyingDetail = new LinkedList<>();
    }

    public Employee getEmployee() {
        return employee;
    }

    public Customer getCustomer() {
        return customer;
    }

    public List<BuyingDetail> getBuyingDetail() {
        return buyingDetail;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setEmployee(Employee employee) throws Exception {
        if (employee == null) {
            throw new IllegalArgumentException("El Empleado no puede ser nulo.");
        }
        
        this.employee = employee;
    }

    public void setCustomer(Customer customer) throws Exception {
        if (customer == null) {
            throw new IllegalArgumentException("El Cliente no puede ser nulo.");
        }
        
        this.customer = customer;
    }

    public void setBuyingDetail(LinkedList<BuyingDetail> buyingDetail) throws Exception {
        if (buyingDetail == null) {
            throw new IllegalArgumentException("Los Detalles de compras no pueden ser nulos.");
        }
        
        this.buyingDetail = buyingDetail;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
