/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import interfaz.GUI_Store;
import java.util.logging.Level;
import java.util.logging.Logger;
import store.Customer;
import store.Employee;
import store.Gender;
import store.Store;
import store.ProductType;
import store.Product;
/**
 *
 * @author Stuard Romero
 */
public class Main {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GUI_Store.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GUI_Store.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GUI_Store.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GUI_Store.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                
                try {
                    Store store = new Store("10101010", "Almacen Los Delicias", "Carrera 29b");
                    //Lista de Tipos de Productos predefinida:
//                    ProductType[] productTypes = {
//                        new ProductType("Frutas y Verduras", 0.0),
//                        new ProductType("Electronicos", 0.19),
//                        new ProductType("Licores", 0.01)
//                    };
//                    
//                    //Agregar Lista.
//                    for (ProductType productType : productTypes) {
//                        store.addProductType(productType);
//                    }
                    //Lista de Productos predefinida:
                    Product[] products = {
                        new Product(12345678, "Manzana", 2800, store.searchForProductType("Frutas y Verduras")),
                        new Product(87654321, "Portatil", 1100000, store.searchForProductType("Electronicos")),
                        new Product(11111111, "Ron Añejo", 1000, store.searchForProductType("Licores"))
                    };
                    //Agregar Lista.
                    for (Product product : products) {
                        store.addProduct(product);
                    }
                    
                    Employee user = new Employee(25874668, "Stuard", "Romero", Gender.MALE, "root", "1234");
                    store.addEmployee(user);
                    
                    store.addCustomer(new Customer(1234567, "Stephania", "Romero", Gender.FEMALE, 3184074746L, "arboleda.stephania@gmail.com"));


                    new GUI_Store(store, user).setVisible(true);
//A partir de aquí, las ventanas se podrán abrir directamente desde esta ventana principal.
                } catch (Exception ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
}   
